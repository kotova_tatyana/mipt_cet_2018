#include <iostream>
#include <cmath>

using namespace std;

class Complex{
private:
	double Re;
	double Im;
public:
	Complex(){
		Re = 0.;
		Im= 0.;
	};

	Complex(double a, double b){
		Re = a;
		Im = b;
	};

	Complex(const Complex& comp){
		Re = comp.Re;
		Im = comp.Im;
	};

	double real() {
		return Re;
	}

	double image(){
		return Im;
	}

	void operator~ (){
		Im *= -1.;
	}

	void operator- (){
		Re *= -1.;
		Im *= -1.;
	}

	double abs(){
		return sqrt(Re * Re + Im * Im);
	}

	Complex polar(){
		return {abs(), atan(Im / Re)};
	}

	Complex operator+ (Complex& add){
		return {add.Re + Re, add.Im + Im};
	}

	Complex operator- (Complex& sub){
		return {Re - sub.real(), Im - sub.image()};
	}

	Complex operator* (Complex& mult){
		return {Re * mult.Re - Im * mult.Im, Im * mult.Re + Re * mult.Im};
	}

	Complex operator/ (Complex& div){
		if (div.abs() == 0.)
			throw (logic_error("Don't try to divide by 0"));
		return {(Re * div.Re + Im * div.Im) / (div.abs() * div.abs()), (Im * div.Re - Re * div.Im) / (div.abs() * div.abs())};
	}

	friend ostream& operator<< (ostream& out, Complex number){
		if (number.Im > 0.)
			out << number.Re << " + " << number.Im << "i" << endl;
		else if (number.Im < 0.)
			out << number.Re << " - " << -number.Im << "i" << endl;
		else
			out << number.Re << endl;
		return out;
	}

	/*friend istream& operator>> (istream& in, Complex& number){
		in >> number.Re >> number.Im;
		return in;
	}*/
};
